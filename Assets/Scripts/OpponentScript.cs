﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class OpponentScript : MonoBehaviour
{
    float counter = 3;
    public float speed = 0.001f;
    Animator anim;
    bool isRotate = false;
    bool seeHorizontalObstacle = false;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        counter -= Time.deltaTime;
        if (counter < 0)
        {
            anim.SetBool("isRunning", true);
            gameObject.transform.position += new Vector3(speed, 0, 0 * Time.deltaTime);
        }

        if (isRotate)
        {
            gameObject.transform.position -= new Vector3(0, 0, 0.02f);
        }

        if (seeHorizontalObstacle)
        {
            gameObject.transform.position += new Vector3(0, 0, 0.01f);
        }

        QuitOnEscape();
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "StaticObstacle")
        {
            isRotate = true;
        }

        if (other.gameObject.tag == "HorizontalObstacle")
        {
            seeHorizontalObstacle = true;
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "StaticObstacle")
        {
            isRotate = false;
        }

        if (other.gameObject.tag == "HorizontalObstacle")
        {
            seeHorizontalObstacle = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "RotatingPlatform")
        {
            gameObject.transform.position += new Vector3(0, 0, 0.01f);
        }

        if(collision.gameObject.tag == "FinishTile")
        {
            Destroy(GameObject.FindGameObjectWithTag("Girl"));
        }

        if (collision.gameObject.tag == "ExitZone")
        {
            Restart();
        }

    }

    void Restart()
    {
        gameObject.transform.position = new Vector3(-1.276f, 0.068f, -0.234f);
        gameObject.transform.rotation = Quaternion.Euler(0, 90, 0);
    }

    void QuitOnEscape()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
