﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public Vector3 mesafe;
    public bool thirdPerson = true;
    void Start()
    {
        mesafe = gameObject.transform.position - GameObject.FindGameObjectWithTag("Boy").transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        if (thirdPerson)
        {
            gameObject.transform.position = GameObject.FindGameObjectWithTag("Boy").transform.position + mesafe;
        }
    }
}
