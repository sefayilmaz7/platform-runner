﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingPlatformScript : MonoBehaviour
{
    // Start is called before the first frame update
    public float speedRotate = 1;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        rotateZ();
    }

    public void rotateZ()
    {
        transform.Rotate(xAngle: 0, yAngle: 0, zAngle: speedRotate);
    }
}
