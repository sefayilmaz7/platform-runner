﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Timers;

public class CharacterScript : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 0.01f;
    public float runSpeed = 0.03f;
    Animator anim;
    Rigidbody rb;
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        bool forwardPress = Input.GetKey(KeyCode.W);
        bool rightPress = Input.GetKey(KeyCode.D);
        bool leftPress = Input.GetKey(KeyCode.A);
        bool spacePress = Input.GetKey(KeyCode.Space);

        if (forwardPress)
        {
            Walk();
        }
        if (!forwardPress)
        {
            Idle();
        }
        else if (forwardPress && spacePress)
        {
            Run();
        }
        else if (forwardPress && !spacePress)
        {
            anim.SetBool("isRunning", false);
            Walk();
        }
        if (rightPress)
        {
            turnRight();
        }
        if (leftPress)
        {
            turnLeft();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "FinishTile")
        {
            anim.SetBool("isFinish", true);
            showWall();
            zoomCamera();
        }

        if (collision.gameObject.tag == "Wall")
        {
            // anim set bool (charging , true)
            GameObject.FindGameObjectWithTag("MainCamera").transform.position = new Vector3(19.97f, -0.2961599f, -0.185f);
            GameObject.Destroy(GameObject.FindGameObjectWithTag("Boy"));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "StaticObstacle" || other.gameObject.tag == "ExitZone" || other.gameObject.tag == "HorizontalObstacle")
        {
            restart();
        }
    }


    void Walk()
    {
        anim.SetBool("isWalking", true);
        gameObject.transform.position += new Vector3(speed, 0, 0 * Time.deltaTime);
    }

    void Idle()
    {
        anim.SetBool("isWalking", false);
    }

    void Run()
    {
        anim.SetBool("isRunning", true);
        gameObject.transform.position += new Vector3(speed + runSpeed, 0, 0 * Time.deltaTime);
    }

    void turnRight()
    {
        gameObject.transform.position -= new Vector3(0, 0, 0.2f * Time.deltaTime);
    }

    void turnLeft()
    {
        gameObject.transform.position += new Vector3(0, 0, 0.2f * Time.deltaTime);
    }

    public void showWall()
    {
        GameObject.FindGameObjectWithTag("Wall").transform.position = new Vector3(20.82f, -0.289f, -0.261f);
    }

    void zoomCamera()
    {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScript>().thirdPerson = false;
        GameObject.FindGameObjectWithTag("MainCamera").transform.position = new Vector3(19.284f, -0.2961599f, -0.132f);
    }

    void restart()
    {
        gameObject.transform.position = new Vector3(-1.276f, 0.068f, -0.234f);
        gameObject.transform.rotation = Quaternion.Euler(0, 90, 0);
    }

}
