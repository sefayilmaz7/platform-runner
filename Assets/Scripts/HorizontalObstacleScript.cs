﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalObstacleScript : MonoBehaviour
{
    // Start is called before the first frame update
    public float speedRotate = 1;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        rotateY();
    }


    public void rotateY()
    {
        transform.Rotate(xAngle: 0, yAngle: speedRotate, zAngle: 0);
    }
}
